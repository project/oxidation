<?php // $Id$ ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
<div id="header">
	<div id="logo">
      <?php if ($logo) { ?><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
      <?php if ($site_name) { ?><h1><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><h2><?php print $site_slogan ?></h2><?php } ?>
	</div>
	<div class="search">
		<?php if ($search_box): ?><?php print $search_box ?><?php endif; ?>
	</div>
</div>
<!-- end #header -->
<div id="menu">
	<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')) ?><?php } ?>
</div>

<!-- end #menu -->
<div id="page">
	<div id="content">
		<div id="latest-post" class="post">
		<?php print $breadcrumb ?>
		<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php if ($show_messages) { print $messages; } ?>
        <?php print $help ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
		</div>
		<!-- end #latest-post -->
		<div id="left-sidebar">
			<?php if ($left) { ?>
				<?php print $left ?>
			<?php } ?>
		</div>
		<!-- end #recent-posts -->
	</div>
	<!-- end content -->
	<div id="sidebar">
			<?php if ($right) { ?>
				<?php print $right ?>
			<?php } ?>
	</div>
	<!-- end #sidebar -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
<hr />
<div id="footer">
	<?php print $footer_message ?>
	<?php print $footer ?>
	<p class="credit">Design by <a href="http://www.freecsstemplates.org/">Free CSS Templates</a> | Ported by <a href="http://doncoryon.com">DonCoryon</a> for <a href="http://goodwinsolutions.com">Goodwin Web Solutions</a></p>
</div>
<?php print $closure ?>
</body>
</html>
